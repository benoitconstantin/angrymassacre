﻿Shader "Custom/ExplosionGlow"
{
	Properties
	{
		_MainTex("Base (RGB) Trans (A)", 2D) = "white" {}
		_EmitterPosition("Emitter Position", vector) = (0,0,0,0)
		_GlowColor("Glow Color", Color) = (1,1,0,0)
		_GlowAmount("Glow Amount", Range(0,02)) = 0.005
		_DistanceFade("Distance Fade", Range(1.0,10.0)) = 5.0
		_FallOff("Fall Off", Range(1.0,10.0)) = 1.0
	}

	Category
	{
		Tags{ "Queue" = "Transparent" "IgnoreProjector" = "True" "RenderType" = "Opaque" }
		ZWrite Off
		//Alphatest Greater 0
		Blend SrcAlpha OneMinusSrcAlpha
		Fog{ Color(1,1,1,0) }
		Lighting Off
		Cull Off //we can turn backface culling off because we know nothing will be facing backwards

		BindChannels
		{
			Bind "Vertex", vertex
			Bind "texcoord", texcoord
			//Bind "Color", color
		}

		SubShader
		{
			Pass
			{
				//SetTexture [_MainTex] 
				//{
				//	Combine texture * primary
				//}



				CGPROGRAM
				#pragma vertex vert
				#pragma fragment frag
				#include "UnityCG.cginc"
				#pragma profileoption NumTemps=64
				//float4 _Color;
				float4 _GlowColor;
				sampler2D _MainTex;
				float _GlowAmount;
				uniform float4 _EmitterPosition;
				float _DistanceFade;
				float _FallOff;

				struct v2f
				{
					float4  pos : SV_POSITION;
					float2  uv : TEXCOORD0;
					float4 position_in_worldspace : TEXCOORD1;
				};

				float4 _MainTex_ST;

				v2f vert(appdata_base v)
				{
					v2f o;
					o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
					o.uv = TRANSFORM_TEX(v.texcoord, _MainTex);
					o.position_in_worldspace = mul(unity_ObjectToWorld, v.vertex);
					return o;
				}

				half4 frag(v2f i) : COLOR
				{

					//half4 texcol = tex2D (_MainTex, i.uv);
					//return texcol * _Color;

					half4 texcol = tex2D(_MainTex, float2(i.uv.x, i.uv.y));
					float dist = distance(i.position_in_worldspace, _EmitterPosition);
					float dNorm = dist / _DistanceFade;
					dNorm = pow(dNorm, _FallOff);

					if (texcol.a<0.5)
					{
						texcol = half4(0.0,0.0,0.0,0.0);
						float remaining = 1.0f;
						float coef = 1.0;
						float fI = 0;
						for (int j = 0; j < 2; j++)
						{
							fI++;
							coef *= 0.32;
							texcol += tex2D(_MainTex, float2(i.uv.x, i.uv.y - fI * _GlowAmount)) * coef;
							texcol += tex2D(_MainTex, float2(i.uv.x - fI * _GlowAmount, i.uv.y)) * coef;
							texcol += tex2D(_MainTex, float2(i.uv.x + fI * _GlowAmount, i.uv.y)) * coef;
							texcol += tex2D(_MainTex, float2(i.uv.x, i.uv.y + fI * _GlowAmount)) * coef;

							remaining -= 4 * coef;
						}
						texcol += tex2D(_MainTex, float2(i.uv.x, i.uv.y)) * remaining;
						//texcol.r+=1;
						//texcol +=_GlowColor;
						texcol += -_GlowColor;
						texcol.r = 1 - texcol.r;
						texcol.g = 1 - texcol.g;
						texcol.b = 1 - texcol.b;
						texcol.a += min(1 - dNorm, texcol.a);
					}
					else
					{
						texcol = half4(0.0, 0.0, 0.0, 0.0);
						float remaining = 1.0f;

						texcol += tex2D(_MainTex, float2(i.uv.x, i.uv.y)) * remaining;
						texcol.a += min(1 - dNorm, texcol.a);
					}

					//texcol.r=1;

					return texcol;
				}
				ENDCG
			}
		}
	}
}
