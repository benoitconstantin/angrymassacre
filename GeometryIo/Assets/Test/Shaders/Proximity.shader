﻿// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

Shader "Custom/Proximity" {
	Properties
	{
		_Color("Color", Color) = (1,1,1,1)
		_MainTex("Base (RGB)", 2D) = "white" {} // Regular object texture 
	_PlayerPosition("Player Position", vector) = (0,0,0,0) // The location of the player - will be set by script
		_VisibleDistance("Visibility Distance", float) = 10.0 // How close does the player have to be to make object visible
		_OutlineWidth("Outline Width", float) = 1.0 // Used to add an outline around visible area a la Mario Galaxy
		_FallOff("Fall Off", Range(1.0, 10.0)) = 1.0
	}
		SubShader
	{
		Tags{ "RenderType" = "Transparent" "Queue" = "Transparent" }
		LOD 100
		Pass{
		Blend SrcAlpha OneMinusSrcAlpha

		CGPROGRAM
#pragma vertex vert
#pragma fragment frag

#include "UnityCG.cginc"


		// Access the shaderlab properties
		uniform fixed4 _Color;
	uniform sampler2D _MainTex;
	uniform float4 _PlayerPosition;
	uniform float _VisibleDistance;
	uniform float _OutlineWidth;
	uniform fixed4 _OutlineColour;

	float4 _MainTex_ST;
	float _FallOff;


	// Input to vertex shader
	struct vertexInput {
		float4 vertex : POSITION;
		float4 texcoord : TEXCOORD0;
	};
	// Input to fragment shader
	struct vertexOutput {
		float4 pos : SV_POSITION;
		float4 position_in_world_space : TEXCOORD0;
		float4 tex : TEXCOORD1;
	};

	// VERTEX SHADER
	vertexOutput vert(vertexInput input)
	{
		vertexOutput output;
		output.pos = mul(UNITY_MATRIX_MVP, input.vertex);
		output.position_in_world_space = mul(unity_ObjectToWorld, input.vertex);
		output.tex = input.texcoord;

		output.tex.xy = TRANSFORM_TEX(input.texcoord, _MainTex);

		return output;
	}

	// FRAGMENT SHADER
	fixed4 frag(vertexOutput input) : COLOR
	{
		// Calculate distance to player position
		float dist = distance(input.position_in_world_space, _PlayerPosition);
	float dNorm = dist / (_VisibleDistance + _OutlineWidth);
	dNorm = pow(dNorm, _FallOff);

	// Return appropriate colour
	if (dist < _VisibleDistance) {
		fixed4 tex = tex2D(_MainTex, input.tex); // Visible
		tex = tex * _Color;
		tex.a = min(1 - dNorm, tex.a);
		return tex;
	}
	else if (dist < _VisibleDistance + _OutlineWidth)
	{

		return float4 (_Color.xyz,  1 - dNorm); // Edge of visible range
	}
	else {
		fixed4 tex = tex2D(_MainTex, input.tex); // Outside visible range
		tex.a = 0;
		return tex;
	}
	}

		ENDCG
	} // End Pass
	} // End Subshader
		FallBack "Diffuse"
} // End Shader