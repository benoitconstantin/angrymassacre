﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosionManager : MonoBehaviour
{

    public Material m_Material;

    // Use this for initialization
    void Start()
    {
        Color randColor = Color.HSVToRGB(Random.Range(0f, 1f), 1f, 1f);
        randColor[3] = 0f;
        m_Material.SetColor("_GlowColor", randColor);
    }
}
