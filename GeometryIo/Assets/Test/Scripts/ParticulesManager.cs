﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticulesManager : MonoBehaviour
{

    public Material m_Material;

    // Use this for initialization
    void Start ()
    {
        Color color = Color.HSVToRGB(Random.Range(0f, 1f), 1f, 1f);
        color[3] = 1f;
        m_Material.SetColor("_TintColor", color);
    }
}
