﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerPositionShaderUpdate : MonoBehaviour {

    [ExecuteInEditMode]

    public Material m_Material;
    //public Transform m_PlayerTransform;

    void Update()
    {
        // Pass the player location to the shader
        if (Ship.playableShip != null)
        {
            m_Material.SetVector("_PlayerPosition", Ship.playableShip.shipmove.charactercontroller2D.CharacterTransform.position);
        }
        /*if (m_PlayerTransform != null) //Debug
        {
            m_Material.SetVector("_PlayerPosition", m_PlayerTransform.position);
        }*/
    }
}
