﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomSpriteColor : MonoBehaviour
{

    public Color color;

	// Use this for initialization
	public Color SetColor()
    {

        color = Color.HSVToRGB(Random.Range(0f, 1f), 1f, 1f);
        color[3] = 1f;
        SpriteRenderer SR = gameObject.GetComponent<SpriteRenderer>();
        SR.color = color;

        return color;
	}

    public void SetColor(Color c)
    {
        SpriteRenderer SR = gameObject.GetComponent<SpriteRenderer>();
        SR.color = c;
    }
}
