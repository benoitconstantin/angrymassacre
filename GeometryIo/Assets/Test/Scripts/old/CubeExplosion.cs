﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeExplosion : MonoBehaviour
{
    public ExplosionParticule m_ExplosionParticulePrefab;
    public int m_NombreParticules = 5;
    public float m_DistanceFromEmitter = 0.2f; // Distance par rapport au cube parent qui explose.
    public float m_ForceFactor = 100;

    private ExplosionParticule[] m_ExplosionParticules;

	// Initialisation
	void Start ()
    {
        m_ExplosionParticules = new ExplosionParticule[m_NombreParticules];
        for (int i = 0; i < m_NombreParticules; i++)
        {        
            m_ExplosionParticules[i] = GetComponentInChildren<ExplosionParticule>();
            ParticulePlacement(m_ExplosionParticules[i]);
            m_ExplosionParticules[i].gameObject.SetActive(false);
        }
	}
	
	// Update
	void Update ()
    {
		if (Input.GetButton("Fire1"))
        {
            Explosion();
        }
	}

    private void ParticulePlacement(ExplosionParticule EP)
    {
        Vector3 tempPrefabPosition = transform.position;
        float tempRandom = Random.Range(0.0f, Mathf.PI * 2);
        tempPrefabPosition.x += Mathf.Cos(tempRandom) * m_DistanceFromEmitter;
        tempPrefabPosition.y += Mathf.Sin(tempRandom) * m_DistanceFromEmitter;
        EP.transform.position = tempPrefabPosition;
    }

    private void Explosion()
    {
        this.gameObject.SetActive(false);
        foreach (ExplosionParticule EP in m_ExplosionParticules)
        {
            EP.gameObject.SetActive(true);
            Vector3 tempPrefabDirection = (EP.transform.position - transform.position).normalized;
            EP.m_RigidBody2d.AddForce(tempPrefabDirection * 100.0f);
        }



        /*ExplosionParticule tempPrefab;

        if (m_ExplosionParticulePrefab != null)
        {
            for (int i = 0; i < m_NombreParticules; i++)
            {

                //tempPrefab = Instantiate(m_ExplosionParticulePrefab, tempPrefabPosition, transform.rotation);

                Vector3 tempPrefabDirection = (tempPrefabPosition - transform.position).normalized;
                //tempPrefab.m_RigidBody2d.AddForce(tempPrefabDirection * 100.0f);

            }
        }
        */
    }

}
