﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosionParticule : MonoBehaviour
{
    private float m_Timer = 0.0f;
    public float m_Lifetime = 0.5f;

    public Rigidbody2D m_RigidBody2d;
    public Material m_Material;
    private Color m_Color1 = Color.white;
    private Color m_Color2;

    // Use this for initialization
    void Start ()
    {
        m_Timer = 0.0f;
        float RandRColor = Random.Range(0f, 1f);
        float RandGColor = Random.Range(0f, 1f);
        float RandBColor = Random.Range(0f, 1f);
        m_Color2 = new Color(RandRColor, RandGColor, RandBColor);
    }
	
	// Update is called once per frame
	void Update ()
    {
		if (m_Timer > m_Lifetime)
        {
            Destroy(this.gameObject);
        }
        else
        {
            m_Timer += Time.deltaTime;
            m_Material.color = Color.Lerp(m_Color1, m_Color2, (m_Timer/m_Lifetime));
        }

        

    }
}
