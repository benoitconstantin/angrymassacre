﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EmitterPositionShaderUpdate : MonoBehaviour {


    // Take effect even in edit mode
    [ExecuteInEditMode]

    public Material m_Material;
    public Transform m_PlayerTransform;

    void Update()
    {
        // Pass the player location to the shader
        if (m_PlayerTransform != null)
        {
            m_Material.SetVector("_EmitterPosition", m_PlayerTransform.position);
        }
    }
}
