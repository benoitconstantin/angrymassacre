﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using System;

namespace EquilibreGames
{
    public class TransformSynchronizer : MovementSynchronizer
    {
        class TransformState : State
        {
            public Vector3 rotation;

            public TransformState(Transform t, int timestamp)
            {
                position = t.position;
                rotation = t.rotation.eulerAngles;

                this.timestamp = timestamp;
            }
        }

        [SerializeField]
        new Transform transform;

        [Space(10)]
        [SerializeField]
        SYNCHRONISATION_MODE positionSynchronizationMode;

        public INTERPOLATION_MODE positionInterpolationMode;

        [Space(20)]
        public float positionThreshold = 0.01f;
        public float snapThreshold = 10f;

        [Space(10)]
        [SerializeField]
        SYNCHRONISATION_MODE rotationSynchronizationMode;

        [SerializeField]
        bool localRotation = true;

        [SerializeField] [Tooltip("Angle in degree for minimum update")]
        float rotationThreshold = 0.1f;

        [Space(20)]
        public bool useExtrapolation = false;
        public float extrapolationTime = 0.5f;

        private Vector3 lastPosition = Vector3.zero;
        private Quaternion lastRotation = Quaternion.identity;

        void Update()
        {
            IsActive = this.transform.gameObject.activeSelf;

            if (currentStatesIndex < 0 || networkIdentity.hasAuthority)
                return;

            State lhs;
            int lhsIndex;
            State rhs;
            float t;
            float firstStateDelay;
            Vector3 val = Vector3.zero;

            GetBestPlayBackState(out firstStateDelay,out lhsIndex, out lhs, out rhs, out t);


            //Extrapolation
            if (useExtrapolation && (lhs == null || rhs == null))
            {

                Vector3 velocity = Vector3.zero;
                t = Mathf.Clamp(firstStateDelay - NetworkingSystem.Instance.synchronisationBackTime, 0, extrapolationTime);

                //POSITION
                val = this.transform.position;

                if (currentStatesIndex > 1)
                    velocity = ((TransformState)statesBuffer[0]).position - ((TransformState)statesBuffer[1]).position;


                GetVector3(positionSynchronizationMode, ref val, ((TransformState)statesBuffer[0]).position
                                                                    + velocity * t );
                this.transform.position = val;


                //ROTATION
                if (localRotation)
                    val = this.transform.localRotation.eulerAngles;
                else
                    val = this.transform.rotation.eulerAngles;

                if (currentStatesIndex > 1)
                    velocity =  ((TransformState)statesBuffer[0]).rotation - ((TransformState)statesBuffer[1]).rotation;
                else
                    velocity = Vector3.zero;

                GetVector3(rotationSynchronizationMode, ref val, ((TransformState)statesBuffer[0]).rotation
                                                                    + velocity * t);

                if(localRotation)
                    this.transform.localRotation = Quaternion.Euler(val);
                else
                    this.transform.rotation = Quaternion.Euler(val);

            }
            else if ((lhs != null && rhs != null))
            {

                //POSITION
                lastPosition = this.transform.position;

                val = this.transform.position;
                if (Vector3.SqrMagnitude(((TransformState)rhs).position - ((TransformState)lhs).position) > (snapThreshold* snapThreshold))
                {
                    GetVector3(positionSynchronizationMode, ref val, ((TransformState)rhs).position);
                }
                else
                {
                    INTERPOLATION_MODE interpolationMode = GetCurrentInterpolationMode(positionInterpolationMode, lhsIndex);

                    switch (interpolationMode)
                    {
                        case INTERPOLATION_MODE.LINEAR:
                            GetVector3(positionSynchronizationMode, ref val, Vector3.Lerp(lhs.position, rhs.position, t)); break;
                        case INTERPOLATION_MODE.CATMULL_ROM:
                            GetVector3(positionSynchronizationMode, ref val, ExtendedMath.CatmullRomInterpolation(statesBuffer[lhsIndex + 1].position, lhs.position, rhs.position, statesBuffer[lhsIndex - 2].position,
                                                                             statesBuffer[lhsIndex + 1].timestamp, lhs.timestamp, rhs.timestamp, statesBuffer[lhsIndex - 2].timestamp, (1f - t) * lhs.timestamp + t * rhs.timestamp));
#if EQUILIBREGAMES_DEBUG
                            ExtendedMath.DrawCatmullRomInterpolation(statesBuffer[lhsIndex + 1].position, lhs.position, rhs.position, statesBuffer[lhsIndex - 2].position,
                                                                             statesBuffer[lhsIndex + 1].timestamp, lhs.timestamp, rhs.timestamp, statesBuffer[lhsIndex - 2].timestamp);
#endif
                            break;
                    }
                }
                val = this.transform.position;

                //ROTATION
                if (localRotation)
                    val = this.transform.localRotation.eulerAngles;
                else          
                    val = this.transform.rotation.eulerAngles;

                GetVector3(rotationSynchronizationMode, ref val, Quaternion.Slerp(Quaternion.Euler(((TransformState)lhs).rotation), Quaternion.Euler(((TransformState)rhs).rotation), t).eulerAngles);


                if (localRotation)
                    this.transform.localRotation = Quaternion.Euler(val);
                else
                    this.transform.rotation = Quaternion.Euler(val);
            }
            else if (IsActive)
            {
                this.transform.position = lastPosition;
                this.transform.rotation = lastRotation;
            }

            lastPosition = this.transform.position;
            lastRotation = this.transform.rotation;
        }

        public override void ResetStatesBuffer()
        {
            base.ResetStatesBuffer();

            lastPosition = this.transform.position;
            lastRotation = this.transform.rotation;
        }

        public override bool NeedToUpdate()
        {
            if ((!base.NeedToUpdate()) ||
                (Time.realtimeSinceStartup < lastUpdateTime) ||
                 ((Vector2.SqrMagnitude(this.transform.position - lastPosition) < positionThreshold * positionThreshold) &&           
                 (localRotation) ? Quaternion.Angle(this.transform.localRotation, lastRotation) < rotationThreshold : Quaternion.Angle(this.transform.rotation, lastRotation) < rotationThreshold) )
                return false;

            return true;
        }


        public override void GetCurrentState(NetworkWriter networkWriter)
        {
            lastRotation = this.transform.rotation;
            lastPosition = this.transform.position;

            SerializeVector3(positionSynchronizationMode, this.transform.position, networkWriter);

            SerializeVector3(rotationSynchronizationMode, (localRotation) ? this.transform.localRotation.eulerAngles :  this.transform.rotation.eulerAngles, networkWriter);

            if (updateRate != 0)
                lastUpdateTime = Time.realtimeSinceStartup + 1f / updateRate;
            else
                lastUpdateTime = Time.realtimeSinceStartup + Mathf.Infinity;
        }


        public override void ReceiveCurrentState(int timestamp, NetworkReader networkReader)
        {
            TransformState newState = new TransformState(this.transform, timestamp);

            UnserializeVector3(positionSynchronizationMode, ref newState.position, networkReader);
            UnserializeVector3(rotationSynchronizationMode, ref newState.rotation, networkReader);

            AddState(newState);
        }

    }
}
