﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace EquilibreGames
{
    public class NetworkMessages
    {
        #region Standards Assets
        public const short LocalClientConnect = 100;
        public const short ClientConnectFromServer = 101;
        public const short ClientReadyFromServer = 102;
        public const short TimeStampSynchronisation = 103;

        public const short TimerSynchronisation = 200;
        public const short TimerUpdate = 201;
        public const short TimerStart = 202;
        public const short TimerStop = 203;
        public const short TimerAvort = 204;

        #endregion;

        #region Custom Game Messages
        public const short NumberOfKillsSync = 300; 

        #endregion;


    }
}
