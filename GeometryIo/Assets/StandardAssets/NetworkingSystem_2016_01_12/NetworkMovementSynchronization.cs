﻿using UnityEngine;
using UnityEngine.Networking;
using System;
using System.Collections.Generic;

namespace EquilibreGames
{
    /// <summary>
    /// Network behaviour of CharacterController2D class
    /// The channel 4 would be StateUpdate
    /// </summary>
    public class NetworkMovementSynchronization : NetworkBehaviour
    {

        [SerializeField]
        NetworkIdentity networkIdentity;

        [Tooltip("Synchronised object will have their movement synchronised.")]
        public MovementSynchronizer[] movementSynchronizers;


        void FixedUpdate()
        {
            if (!hasAuthority)
                return;

            NetworkWriter writer = new NetworkWriter();

            int updateMask = 0;

            if (movementSynchronizers.Length != 0)
            {
                for (int i = 0; i < movementSynchronizers.Length; i++)
                {
                    if (movementSynchronizers[i].NeedToUpdate())
                    {
                        updateMask = updateMask | (1 << i);
                    }
                }
            }

            if (updateMask != 0 || movementSynchronizers.Length == 0)
            {
                writer.Write(NetworkingSystem.Instance.ServerTimestamp);

                if (movementSynchronizers.Length != 0)
                {
                    if (movementSynchronizers.Length <= 4)
                        writer.Write((byte)updateMask);
                    else if (movementSynchronizers.Length <= 8)
                        writer.Write((short)updateMask);
                    else
                        writer.Write(updateMask);
                }

                for (int i = 0; i < movementSynchronizers.Length; i++)
                {
                    if ((updateMask & (1 << i)) != 0 || movementSynchronizers.Length == 0)
                    {
                        movementSynchronizers[i].GetCurrentState(writer);
                    }
                }

                SendMovementsInformations(writer.ToArray());
            }
        }


        void SendMovementsInformations(byte[] info)
        {
            if (isServer)
                RpcSendMovementsInformations(info);
            else
                CmdSendMovementsInformations(info);
        }


        [ClientRpc(channel = 2)]
        void RpcSendMovementsInformations(byte[] info)
        {
            if (hasAuthority)
                return;

            NetworkReader reader = new NetworkReader(info);
            int timestamp = reader.ReadInt32();
            int updateMask = 0;

            if (movementSynchronizers.Length != 0)
            {
                if (movementSynchronizers.Length <= 4)
                    updateMask = reader.ReadByte();
                else if (movementSynchronizers.Length <= 8)
                    updateMask = reader.ReadInt16();
                else
                    updateMask = reader.ReadInt32();
            }

            while(reader.Position < reader.Length-1)
            {
                for(int i = 0; i < movementSynchronizers.Length; i++)
                {
                    if ((updateMask & (1 << i)) != 0 || movementSynchronizers.Length == 0)
                    {
                        movementSynchronizers[i].ReceiveCurrentState(timestamp, reader);
                    }
                }
            }
        }


        [Command(channel = 2)]
        void CmdSendMovementsInformations(byte[] info)
        {
            RpcSendMovementsInformations(info);
        }
    }

}
