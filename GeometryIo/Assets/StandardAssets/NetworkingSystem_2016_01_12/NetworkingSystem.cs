﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using UnityEngine.Networking.NetworkSystem;
using UnityEngine.Networking.Match;
using UnityEngine.Networking.Types;
using System;
using UnityEngine.SceneManagement;
using System.Collections.Generic;

namespace EquilibreGames
{
    public class NetworkingSystem : Singleton<NetworkingSystem> {

        public bool destroyNetworkObjectsOnDisconnection = true;

        public struct ConfigurationInfo
        {
            public short message;
            public NetworkMessageDelegate func;

            public ConfigurationInfo(short message, NetworkMessageDelegate func)
            {
                this.message = message;
                this.func = func;
            }
        }


        /// <summary>
        /// Client configurations are added to client to handle messages.
        /// </summary>
        public static List<ConfigurationInfo> clientConfigurations = new List<ConfigurationInfo>();


        /// <summary>
        /// Server configurations are added to client to handle messages.
        /// </summary>
        public static List<ConfigurationInfo> serverConfigurations = new List<ConfigurationInfo>();


        [Tooltip("Has the NetworkingSystem to autoConnect on local network on start ?")]
        public bool connectOnStart = true;

        [Tooltip("Has the NetworkingSystem to autoReconnect on local network when a connection with the server is lost ?")]
        public bool autoReconnectOnLocal = true;

        [Tooltip("The server adress to use(By default : local network)")]
        public string serverAdress = "localhost";


        [Tooltip("The server port (By default : 7777 with +1 of increment if the port is busy)")]
        public int serverPort = 7777;


        [Space(20)]
        [Tooltip("Max player allowed in a match")]
        public uint maxPlayer = 5;


        [Space(20)]
        [SerializeField]
        [Tooltip ("Additionnal channels for user. Base channels are :\n0-ReliableSequenced\n1-Reliable\n2-Unreliable\n3-AllCostDelivery\n4-ReliableStateUpdate\n5-StateUpdate")]
        private QosType[] additionnalChannel;

        [Space(20)]
        public uint connectionTimeOut = 1000;
        public uint disconnectionTimeOut = 5000;
    

        [Space(20)]
        public ushort packetSize = 512;
        public ushort fragmentSize = 128;
        public float maxDelayForSendingData = 0.05f;
        public ushort maxSentMessageQueueSize = 100;


        [Space(20)]
        [SerializeField]
        [Tooltip("The GameObject that represent the player")]
        GameObject playerPrefab;

        public bool instantiatePlayerObjectOnClientConnect = true;

        /// <summary>
        /// Networked GameObject which need to be registered
        /// </summary>
        [Space(2)]
        [SerializeField]
        GameObject[] registeredNetworkedGameObjectsArray;

        [Space(10)]
        public float synchronisationBackTime = 0.1f;


        public int ServerTimestamp
        {
            get { if (!isNetworkActive) return 0; return NetworkTransport.GetNetworkTimestamp() + timestampDelta; }
        }
        private int timestampDelta = 0;

        private bool currentMatchIsLocked;
        public bool CurrentMatchIsLocked
        {
            get { return currentMatchIsLocked; }
            set { currentMatchIsLocked = value; }
        }


        void Start()
        {
            if (connectOnStart)
                StartHost();
        }


        public NetworkClient client = null;

        /// <summary>
        /// The client used by the networkSystem.
        /// </summary>
        public NetworkClient Client
        {
            get { return client; }
        }


        #region Server
        /// <summary>
        /// Called on server when a player gameObject is added by the server
        /// </summary>
        public static NetworkMessageDelegate OnServerAddPlayer;

        /// <summary>
        /// Called on server when a client connect
        /// </summary>
        public static NetworkMessageDelegate OnServerConnect;

        /// <summary>
        /// Called on server when a client disconnect
        /// </summary>
        public static NetworkMessageDelegate OnServerDisconnect;

        /// <summary>
        /// Called on server when a client become ready on server
        /// </summary>
        public static NetworkMessageDelegate OnClientReadyOnServer;

        /// <summary>
        /// Called on server when the server start
        /// </summary>
        public static NetworkMessageDelegate OnStartServer;

        /// <summary>
        /// Called on server when the server stop
        /// </summary>
        public static NetworkMessageDelegate OnStopServer;

        #endregion

        #region From server to every client
        /// <summary>
        /// Called on every client when a client connect
        /// </summary>
        public static NetworkMessageDelegate OnClientConnectFromServer;

        /// <summary>
        /// Called on every client when a client become ready on the server
        /// </summary>
        public static NetworkMessageDelegate OnClientReadyFromServer;
        #endregion

        #region Client
        /// <summary>
        /// Called on client when the local client disconnect
        /// </summary>
        public static NetworkMessageDelegate OnClientDisconnect;

        /// <summary>
        /// Called on client when the local client connect
        /// </summary>
        public static NetworkMessageDelegate OnClientConnect;

        /// <summary>
        /// Called on client when the local client become not ready
        /// </summary>
        public static NetworkMessageDelegate OnClientNotReady;


        /// <summary>
        /// Called on client when the local client stop volontary
        /// </summary>
        public static NetworkMessageDelegate OnStopClient;


        #endregion



        public override void OnCreation()
        {

        }


        /// <summary>
        /// Tell if the network is active (localhost or online)
        /// </summary>
        public static bool isNetworkActive
        {
            get { return NetworkClient.active || NetworkServer.active; }
        }

        /// <summary>
        /// Tell if the matchMode is active (online)
        /// </summary>
        private static bool matchMode;
        public static bool isMatchModeActive
        {
            get { return matchMode; }
        }


        /// <summary>
        /// Tell if the lanMode is active
        /// </summary>
        private static bool LANMode;
        public static bool isLANModeActive
        {
            get { return LANMode; }
        }


        /// <summary>
        /// Start Server And Client
        /// </summary>
        /// <param name="matchInfo">if null, will not use relay but will connect locally</param>
        /// <returns></returns>
        public NetworkClient StartHost(MatchInfo matchInfo)
        {
            StartServer(matchInfo);
            StartLocalClient();

            return client;
        }

        /// <summary>
        /// Start Server And Client
        /// </summary>
        /// <returns></returns>
        public NetworkClient StartHost(bool LAN = false, bool matchMode = false)
        {
            StartServer(LAN,matchMode);
            StartLocalClient();

            return client;
        }


        /// <summary>
        /// Start a client using serverAdress and serverPort
        /// </summary>
        /// <returns></returns>
        public NetworkClient StartClient(bool matchMode = false)
        {
            return StartClient(serverAdress, serverPort, matchMode);
        }

        /// <summary>
        /// Start a client using match
        /// </summary>
        /// <param name="matchInfo">if null, will start client with no relay</param>
        /// <returns></returns>
        public NetworkClient StartClient(MatchInfo matchInfo)
        {
            Debug.Log("Start client");

            if (client != null)
                StopClient();
            
            client = new NetworkClient();
            ConfigureClient(client);

            if (client == null) // A Problem occured when the client was configured.
                return null;

            client.Connect(matchInfo);
            matchMode = true;

            return client;
        }


        /// <summary>
        /// Start a client using specific serverAdress and serverPort
        /// </summary>
        /// <param name="matchInfo">if null, will start client with no relay</param>
        /// <returns></returns>
        public NetworkClient StartClient(string serverAdress, int serverPort, bool online = false)
        {
            Debug.Log(serverAdress);
            Debug.Log(serverPort);
            Debug.Log("Start client");

            if (client != null)
                StopClient();

            client = new NetworkClient();
            ConfigureClient(client);

            if (client == null) // A Problem occured when the client was configured.
                return null;

            matchMode = online;
            LANMode = !online;

            client.Connect(serverAdress, serverPort);
  
            return client;
        }




        /// <summary>
        /// Start LocalClient (for the host)
        /// </summary>
        public NetworkClient StartLocalClient()
        {
            if (client != null)
                StopClient();

            Debug.Log("Start local client");

            client = ClientScene.ConnectLocalServer();
            ConfigureClient(client);

            return client;
        }

        /// <summary>
        /// Start a new server
        /// </summary>
        /// <param name="matchInfo"> if null, will start a server with no relay</param>
        public void StartServer(MatchInfo matchInfo)
        {
            if (NetworkServer.active)
                StopServer();

            if(!NetworkTransport.IsStarted)
                NetworkTransport.Init();

            ConfigureServer();

            NetworkServer.ListenRelay(matchInfo.address, matchInfo.port, matchInfo.networkId, SourceID.Invalid, matchInfo.nodeId);
            matchMode = true;

            NetworkServer.SpawnObjects();

            if (OnStartServer != null)
                OnStartServer(null);
        }

        public void StartServer(bool LAN = false, bool online = false)
        {
            if (NetworkServer.active)
                StopServer();

            if (!NetworkTransport.IsStarted)
                NetworkTransport.Init();

            ConfigureServer();

            matchMode = online;
            LANMode = !online;

            if (!LAN)
            {
                while (!NetworkServer.Listen(serverAdress, serverPort))
                {
                    serverPort++;
                }
            }
            else
            {
                while (!NetworkServer.Listen(serverPort))
                {
                    serverPort++;
                }
            }

            NetworkServer.SpawnObjects();

            if (OnStartServer != null)
                OnStartServer(null);
        }


        public void ConfigureClient(NetworkClient client)
        {

            client.RegisterHandler(MsgType.Connect, BaseOnClientConnect);
            client.RegisterHandler(MsgType.Disconnect, BaseOnClientDisconnect);
            client.RegisterHandler(MsgType.AddPlayer, BaseOnClientAddPlayer);
            client.RegisterHandler(MsgType.Scene, BaseOnClientChangeScene);
            client.RegisterHandler(MsgType.NotReady, BaseOnClientNotReady);

            client.RegisterHandler(NetworkMessages.ClientConnectFromServer, BaseOnClientConnectFromServer);
            client.RegisterHandler(NetworkMessages.ClientReadyFromServer, BaseOnClientReadyFromServer);

            client.RegisterHandler(NetworkMessages.TimeStampSynchronisation, BaseOnTimestampSynchronisation);

            foreach (ConfigurationInfo i in clientConfigurations)
                client.RegisterHandler(i.message, i.func);


            client.Configure(Configuration(), (int)maxPlayer);

            if(playerPrefab)
                ClientScene.RegisterPrefab(playerPrefab);

            foreach (GameObject i in registeredNetworkedGameObjectsArray)
                ClientScene.RegisterPrefab(i);
        }

        public void ConfigureServer()
        {
            NetworkServer.RegisterHandler(MsgType.Connect, BaseOnServerConnect);
            NetworkServer.RegisterHandler(MsgType.AddPlayer, BaseOnServerAddPlayer);
            NetworkServer.RegisterHandler(MsgType.Disconnect, BaseOnServerDisconnect);
            NetworkServer.RegisterHandler(MsgType.Ready, BaseOnClientReadyOnServer);
            NetworkServer.RegisterHandler(MsgType.Error, BaseOnServerError);

            foreach (ConfigurationInfo i in serverConfigurations)
                NetworkServer.RegisterHandler(i.message, i.func);

            NetworkServer.Configure(Configuration(), (int)maxPlayer);
        }



        /// <summary>
        /// Stop the current client used by the NetworkingSystem
        /// </summary>
        public void StopClient()
        {
            if (client != null)
            {
                Debug.Log("Client stop");

                if (OnStopClient != null)
                    OnStopClient(null);


                if (destroyNetworkObjectsOnDisconnection)
                    ClientScene.DestroyAllClientObjects();

                    client.Disconnect();
                    client.Shutdown();
                    client = null;
            }
        }


        /// <summary>
        /// Stop the current server used by the NetworkingSystem
        /// </summary>
        public void StopServer()
        {
            BaseOnStopServer();

            NetworkServer.Shutdown();
            NetworkServer.Reset();
        }


        /// <summary>
        /// Stop the current server and local client used by the NetworkingSystem
        /// </summary>
        public void StopHost()
        {
            if (NetworkServer.active)
            {
                StopServer();
            }
            if (NetworkClient.active)
            {
                StopClient();
            }

        }



       public ConnectionConfig Configuration()
        {
            ConnectionConfig config = new ConnectionConfig();

            config.AddChannel(QosType.ReliableSequenced);
            config.AddChannel(QosType.Reliable);
            config.AddChannel(QosType.Unreliable);
            config.AddChannel(QosType.AllCostDelivery);
            config.AddChannel(QosType.ReliableStateUpdate);
            config.AddChannel(QosType.StateUpdate);

            if (additionnalChannel != null)
            {
                foreach (QosType i in additionnalChannel)
                    config.AddChannel(i);
            }

            config.ConnectTimeout = connectionTimeOut;
            config.DisconnectTimeout = disconnectionTimeOut;
            config.PacketSize = packetSize;
            config.FragmentSize = fragmentSize;
            config.MaxSentMessageQueueSize = maxSentMessageQueueSize;

            return config;
        }


        string loadingSceneName = null;

        public void ServerChangeScene(string sceneName)
        {
            Debug.Log("ServerChangeScene " + sceneName);

            loadingSceneName = sceneName;
            //NetworkServer.SetAllClientsNotReady(); //It's fucked up, never mind.

            SceneManager.sceneLoaded += BaseOnServerChangeScene;
            SceneManager.LoadScene(sceneName);

            StringMessage msg = new StringMessage(sceneName);
            NetworkServer.SendToAll(MsgType.Scene, msg);    
        }


        #region Server handlers

        void BaseOnServerConnect(NetworkMessage netMsg)
        {      
            Debug.Log("Server connect");

            if (OnServerConnect != null)
                OnServerConnect(netMsg);

            NetworkServer.SendToAll(NetworkMessages.ClientConnectFromServer, new EmptyMessage());
        }


        void BaseOnServerAddPlayer(NetworkMessage netMsg)
        {
            Debug.Log("Server add player");

            GameObject obj = Instantiate(playerPrefab, Vector3.zero, Quaternion.identity) as GameObject;
            NetworkServer.AddPlayerForConnection(netMsg.conn, obj, 0);

            if (OnServerAddPlayer != null)
                OnServerAddPlayer(netMsg);
        }


        void BaseOnServerError(NetworkMessage netMsg)
        {
           NetworkServer.DestroyPlayersForConnection(netMsg.conn);
        }

        void BaseOnServerDisconnect(NetworkMessage netMsg)
        {
            Debug.Log("Server disconnect");

           NetworkServer.DestroyPlayersForConnection(netMsg.conn);

            if (OnServerDisconnect != null)
                OnServerDisconnect(netMsg);
        }


        void BaseOnClientConnectFromServer(NetworkMessage netMsg)
        {
            Debug.Log("Client connect from server");

            if (OnClientConnectFromServer != null)
                OnClientConnectFromServer(netMsg);
        }

      
        void BaseOnClientReadyOnServer(NetworkMessage netMsg)
        {
            Debug.Log("Client ready on server");

            NetworkServer.SetClientReady(netMsg.conn);
            NetworkServer.SendToClient(netMsg.conn.connectionId, NetworkMessages.TimeStampSynchronisation, new IntegerMessage(NetworkTransport.GetNetworkTimestamp()));

            if (OnClientReadyOnServer != null)
                OnClientReadyOnServer(netMsg);

            if (OnClientReadyFromServer != null)
                OnClientReadyFromServer(netMsg);

            NetworkServer.SendToAll(NetworkMessages.ClientReadyFromServer, new EmptyMessage());
        }


        void BaseOnStopServer()
        {
            Debug.Log("Server stop");

            if (OnStopServer != null)
                OnStopServer(null);
        }


        void BaseOnServerChangeScene(Scene scene, LoadSceneMode mode)
        {
            if (loadingSceneName.Equals(scene.name))
            {
                SceneManager.sceneLoaded -= BaseOnServerChangeScene;
                NetworkServer.SpawnObjects();
                loadingSceneName = null;
            }
        }


        void BaseOnTimestampSynchronisation(NetworkMessage netMsg)
        {
            byte error;

            if (!NetworkServer.active)
            {
                timestampDelta = netMsg.ReadMessage<IntegerMessage>().value;
                timestampDelta += NetworkTransport.GetRemoteDelayTimeMS(client.connection.hostId, client.connection.connectionId, timestampDelta, out error);
                timestampDelta = timestampDelta - NetworkTransport.GetNetworkTimestamp();
            }
            else
                timestampDelta = 0;

        }

        public static int ServerConnectionsCount()
        {
            int cpt = 0;

            foreach(NetworkConnection i in NetworkServer.connections)
            {
                if (i != null)
                    cpt++;
            }

            return cpt;
        }

        #endregion


        #region Client handlers

        void BaseOnClientConnect(NetworkMessage netMsg)
        {
            Debug.Log("Client connect");

            client.connection.SetMaxDelay(maxDelayForSendingData);

           // System.Threading.Thread.Sleep(1000);

            if (OnClientConnect != null)
                OnClientConnect(netMsg);
        }

         void BaseOnClientAddPlayer(NetworkMessage netMsg)
        {

        }


        void BaseOnClientReadyFromServer(NetworkMessage netMsg)
        {
            if (NetworkServer.active)
                return;

            Debug.Log("Client ready from server");

            if(netMsg.conn == ClientScene.readyConnection)
            {
                if (instantiatePlayerObjectOnClientConnect)
                {
                    ClientScene.AddPlayer(0);
                }
            }

            if (OnClientReadyFromServer != null)
                OnClientReadyFromServer(netMsg);
        }

        void BaseOnClientNotReady(NetworkMessage netMsg)
        {
            Debug.Log("Client set as not ready");
           
            if (OnClientNotReady != null)
                OnClientNotReady(netMsg);
        }

         void BaseOnClientDisconnect(NetworkMessage netMsg)
        {
            ClientScene.DestroyAllClientObjects();
            NetworkClient.ShutdownAll();
            client = null;

            Debug.Log("Client disconnect");

            matchMode = false;

            if (OnClientDisconnect != null)
                OnClientDisconnect(netMsg);

            if(autoReconnectOnLocal)
                StartHost();
        }


         void BaseOnClientChangeScene(NetworkMessage netMsg)
        {
            string sceneName = netMsg.ReadMessage<StringMessage>().value;

            if (!NetworkServer.active)
            {
                SceneManager.sceneLoaded += BaseOnClientChangeScene;
                SceneManager.LoadScene(sceneName);
            }
        }


        void BaseOnClientChangeScene(Scene scene, LoadSceneMode mode)
        {
            if (scene.Equals(loadingSceneName))
            {
                SceneManager.sceneLoaded -= BaseOnClientChangeScene;
               // ClientScene.Ready(client.connection);
                loadingSceneName = null;
            }
        }

        #endregion



        public override void OnApplicationQuit()
        {
            base.OnApplicationQuit();

            if (NetworkServer.active && NetworkServer.connections.Count == 1)
            {
                StopServer();
            }

            if (NetworkClient.active)
                StopClient();
        }

#if UNITY_EDITOR || EQUILIBRE_GAMES_DEBUG
        void OnGUI()
        {
            GUI.Label(new Rect(Screen.width-200, Screen.height-50, 200, 30), "TimeStamp : "+ ServerTimestamp);
        }
#endif
    }

}
