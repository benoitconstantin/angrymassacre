﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.Networking;

namespace EquilibreGames
{
    
    public abstract class MovementSynchronizer : MonoBehaviour
    {
        public enum SYNCHRONISATION_MODE { NONE, X, Y, Z, XY, XZ, YZ, XYZ };
        public enum SNAP_MODE { NONE, RESET, CALCUL};
        public enum INTERPOLATION_MODE { LINEAR, CATMULL_ROM};

        public class State
        {
            public int timestamp = -1;
            public Vector3 position;

            /// <summary>
            /// bit 1 --> Teleportation
            /// </summary>
            public byte infos;
        }

        [SerializeField]
        [Tooltip("If this is != null, no one can set the velocity except who has authority on the object")]
        protected NetworkIdentity networkIdentity;

        public float updateRate = 30f;

        [SerializeField]
        int maxBufferSize = 60;

        protected State[] statesBuffer;
        protected int currentStatesIndex = -1;
        protected float lastUpdateTime = -1;


        private bool isActive = true;
        public bool IsActive
        {
            get { return isActive; }
            set
            {
                if(value != isActive)
                {
                    if (value)
                        OnActive();
                    else
                        OnInactive();
                }

                isActive = value;
            }
        }

        public abstract void GetCurrentState(NetworkWriter networkWriter);
        public abstract void ReceiveCurrentState(int timeStamp, NetworkReader networkReader);

        public virtual bool NeedToUpdate()
        {
            return isActive && enabled;
        }

        protected virtual void Awake()
        {
            statesBuffer = new State[maxBufferSize];
        }


        void OnActive()
        {
           ResetStatesBuffer();
        }

        void OnInactive()
        {
           // ResetStatesBuffer();
        }

        /// <summary>
        /// Reset the states buffer by putting the currentStatesIndex et it initial value
        /// </summary>
        public virtual void ResetStatesBuffer()
        {
            currentStatesIndex = -1;
        }

        public void AddState(State s)
        {
            //If no States are present, put in first slot.
            if (currentStatesIndex < 0)
            {
                statesBuffer[0] = s;
            }
            else
            {
                //First find proper place in buffer. If no place is found, state can be dropped (newState is too old)
                for (int i = 0; i <= currentStatesIndex; i++)
                {
                    //If the state in slot i is older than our new state, we found our slot.  
                    if (statesBuffer[i].timestamp < s.timestamp)
                    {
                        // Shift the buffer sideways, to make room in slot i. possibly deleting last state
                        for (int k = maxBufferSize - 1; k > i; k--)
                        {
                            statesBuffer[k] = statesBuffer[k - 1];
                        }
                        //insert state
                        statesBuffer[i] = s;

                        //We are done, exit loop
                        break;
                    }
                }
            }

            currentStatesIndex = Mathf.Min(currentStatesIndex + 1, maxBufferSize-1);
        }

        public void GetBestPlayBackState(out float firstStateDelay,out int lhsIndex, out State lhs, out State rhs, out float t)
        {

            byte error;

            lhs = null;
            rhs = null;
            t = -1;
            lhsIndex = 0;

            if (!NetworkServer.active)
                firstStateDelay = NetworkTransport.GetRemoteDelayTimeMS(
                 ClientScene.readyConnection.hostId,
                 ClientScene.readyConnection.connectionId,
                 statesBuffer[0].timestamp,
                out error);
            else
                firstStateDelay = NetworkTransport.GetNetworkTimestamp() - statesBuffer[0].timestamp;

            firstStateDelay /= 1000f;

            float synchronisationBackTime = NetworkingSystem.Instance.synchronisationBackTime;

          //  Debug.Log("firstDelay : " + firstStateDelay / 1000f);
          //  Debug.Log("RTT : " + NetworkingSystem.Instance.client.GetRTT());

            // Use interpolation if the playback time is present in the buffer (it's obligatly less than the firstStateDelay(wich is the younger)
            if (firstStateDelay < synchronisationBackTime)
            {
                // Go through buffer and find correct state to play back
                for (int i = 0; i < currentStatesIndex; i++)
                {
                    // Search the best playback state (closest to 100 ms old (default time))
                    lhs = statesBuffer[i];

                    int lhsDelayMS = -1;

                    if (!NetworkServer.active)
                        lhsDelayMS = NetworkTransport.GetRemoteDelayTimeMS(
                            ClientScene.readyConnection.hostId,
                            ClientScene.readyConnection.connectionId,
                        lhs.timestamp,
                        out error);
                    else
                        lhsDelayMS = NetworkTransport.GetNetworkTimestamp() - lhs.timestamp;


                    //We find a state to synchronise or it's the last state
                    if (lhsDelayMS / 1000f >= synchronisationBackTime || i == currentStatesIndex - 1)
                    {
                        // The state one slot newer (<100ms (default time)) than the best playback state
                        //The goal is to linearly lerp all the time we can !
                        rhs = statesBuffer[Mathf.Max(i - 1, 0)];

                        int rhsDelayMS = -1;

                        if (!NetworkServer.active)
                            rhsDelayMS = NetworkTransport.GetRemoteDelayTimeMS(
                                ClientScene.readyConnection.hostId,
                                ClientScene.readyConnection.connectionId,
                            rhs.timestamp,
                            out error);
                        else
                            rhsDelayMS = NetworkTransport.GetNetworkTimestamp() - rhs.timestamp;


                        lhsIndex = i;
                        t = Mathf.InverseLerp((float)lhsDelayMS / 1000f, (float)rhsDelayMS / 1000f, synchronisationBackTime);
                        break;
                    }
                }
            }
        }

        public void SerializeVector3(SYNCHRONISATION_MODE mode, Vector3 value, NetworkWriter networkWriter)
        {
            switch (mode)
            {
                case SYNCHRONISATION_MODE.X:
                    networkWriter.Write(value.x); break;

                case SYNCHRONISATION_MODE.Y:
                    networkWriter.Write(value.z); break;

                case SYNCHRONISATION_MODE.Z:
                    networkWriter.Write(value.z); break;

                case SYNCHRONISATION_MODE.XY:
                    networkWriter.Write(value.x);
                    networkWriter.Write(value.y); break;

                case SYNCHRONISATION_MODE.XZ:
                    networkWriter.Write(value.x);
                    networkWriter.Write(value.z); break;

                case SYNCHRONISATION_MODE.YZ:
                    networkWriter.Write(value.y);
                    networkWriter.Write(value.z); break;

                case SYNCHRONISATION_MODE.XYZ:
                    networkWriter.Write(value.x);
                    networkWriter.Write(value.y);
                    networkWriter.Write(value.z); break;

                case SYNCHRONISATION_MODE.NONE: break;
            }
        }

        public void SerializeQuatenion(Quaternion value, NetworkWriter networkWriter)
        {
            networkWriter.Write(value);
        }

        public void UnserializeQuaternion(ref Quaternion value, NetworkReader networkReader)
        {
            value = networkReader.ReadQuaternion();
        }

        public void UnserializeVector3(SYNCHRONISATION_MODE mode, ref Vector3 value, NetworkReader networkReader)
        {
            switch (mode)
            {
                case SYNCHRONISATION_MODE.X:
                    value.x = networkReader.ReadSingle(); break;

                case SYNCHRONISATION_MODE.Y:
                    value.y = networkReader.ReadSingle(); break;

                case SYNCHRONISATION_MODE.Z:
                    value.z = networkReader.ReadSingle(); break;

                case SYNCHRONISATION_MODE.XY:
                    value.x = networkReader.ReadSingle();
                    value.y = networkReader.ReadSingle(); break; 

                case SYNCHRONISATION_MODE.XZ:
                    value.x = networkReader.ReadSingle();
                    value.z = networkReader.ReadSingle(); break;

                case SYNCHRONISATION_MODE.YZ:
                    value.y = networkReader.ReadSingle();
                    value.z = networkReader.ReadSingle(); break;

                case SYNCHRONISATION_MODE.XYZ:
                    value.x = networkReader.ReadSingle();
                    value.y = networkReader.ReadSingle();
                    value.z = networkReader.ReadSingle(); break;

                case SYNCHRONISATION_MODE.NONE: break;
            }
        }

        public void GetVector3(SYNCHRONISATION_MODE mode, ref Vector3 value, Vector3 target)
        {
            switch (mode)
            {
                case SYNCHRONISATION_MODE.X:
                    value.x = target.x; break;

                case SYNCHRONISATION_MODE.Y:
                    value.y = target.y; break;

                case SYNCHRONISATION_MODE.Z:
                    value.z = target.z; break;

                case SYNCHRONISATION_MODE.XY:
                    value.x = target.x;
                    value.y = target.y; break;

                case SYNCHRONISATION_MODE.XZ:
                    value.x = target.x;
                    value.z = target.z; break;

                case SYNCHRONISATION_MODE.YZ:
                    value.y = target.y;
                    value.z = target.z; break;

                case SYNCHRONISATION_MODE.XYZ:
                    value.x = target.x;
                    value.y = target.y;
                    value.z = target.z; break;

                case SYNCHRONISATION_MODE.NONE: break;
            }
        }


        /// <summary>
        /// Get the current interpolation Mode to use, based on the baseMode you want to use.
        /// Some interpolation need the respect of different rule to be available
        /// </summary>
        /// <param name="baseMode"></param>
        /// <param name="currentLhsIndex"></param>
        /// <returns></returns>
        protected INTERPOLATION_MODE GetCurrentInterpolationMode(INTERPOLATION_MODE baseMode, int currentLhsIndex)
        {

            switch (baseMode)
            {
                case INTERPOLATION_MODE.CATMULL_ROM:
                    if (currentStatesIndex < 3 || currentLhsIndex == 0 || currentLhsIndex + 1 > currentStatesIndex || currentLhsIndex - 2 < 0 
                        || (statesBuffer[currentLhsIndex].timestamp - statesBuffer[currentLhsIndex + 1].timestamp) > 100f // superior to 100ms the catmull-Rom interpolation can be very wrong.
                        || (statesBuffer[currentLhsIndex].position - statesBuffer[currentLhsIndex + 1].position).sqrMagnitude < 0.0001) // 0.01 position threshold.
                    {
                        return INTERPOLATION_MODE.LINEAR;
                    }
                    break;
            }
            return baseMode;
        }

    }
}
