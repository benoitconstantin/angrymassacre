﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

namespace EquilibreGames
{
    public class NetworkingSpawner : MonoBehaviour {

        [SerializeField]
        GameObject prefab;


        GameObject objSpawned;

        void Awake()
        {
            NetworkingSystem.OnStartServer += SpawnObject;
            NetworkingSystem.OnStopServer += DestroyObject;
        }

        void OnDestroy()
        {
                NetworkingSystem.OnStartServer -= SpawnObject;
                NetworkingSystem.OnStopServer -= DestroyObject;
        }

        void SpawnObject(NetworkMessage netMsg)
        {
            NetworkServer.Spawn(objSpawned = Instantiate(prefab, this.transform.position, this.transform.rotation) as GameObject);
        }


        void DestroyObject(NetworkMessage netMsg)
        {
            NetworkServer.Destroy(objSpawned);
        }
    }
}
