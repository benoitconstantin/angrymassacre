﻿using UnityEngine;
using System.Collections;


namespace EquilibreGames
{
    public class ExtendedMath : MonoBehaviour
    {

        /// <summary>
        /// Find the closest point on the surface of a circle defined by the circle collider 2D
        /// </summary>
        /// <param name="collider">The collider that you analyse</param>
        /// <param name="toPosition">The point That you analyse</param>
        /// <returns>The closesPoint for a circleCollider2D Shape</returns>
        public static Vector2 ClosestPointOnSurface(CircleCollider2D collider, Vector2 toPosition)
        {
            Vector2 positionReturned;

            //Calcul the normal vector
            positionReturned = toPosition - (Vector2)collider.bounds.center;
            positionReturned.Normalize();

            //Multiply the normal by the global radius of the collider (extents.x or extents.y are equal for cercle)
            positionReturned *= collider.bounds.extents.x;


            //Translate the position with the collider position.
            positionReturned += (Vector2)collider.bounds.center;

            return positionReturned;
        }


        /// <summary>
        /// Find the closest point on the surface of a Box2D defined by the box collider 2D. Can be oriented.
        /// </summary>
        /// <param name="collider">The collider that you analyse</param>
        /// <param name="toPosition">The point That you analyse</param>
        /// <returns>The closesPoint for a BoxCollider2D Shape</returns>
        public static Vector2 ClosestPointOnSurface(BoxCollider2D collider, Vector2 toPosition)
        {
            // Cache the collider transform
            var ct = collider.transform;

            // Firstly, transform the point into the space of the collider
            Vector2 local = ct.InverseTransformPoint(toPosition);

            // Now, shift it to be in the center of the box
            local = local - collider.offset;

            //Pre multiply to save operations.
            Vector2 halfSize = collider.size * 0.5f;
            Vector2 worldhalfSize;

            worldhalfSize.x = halfSize.x * ct.lossyScale.x;
            worldhalfSize.y = halfSize.y * ct.lossyScale.y;


            // Clamp the points to the collider's extents
            Vector2 localNorm = new Vector3(
                    Mathf.Clamp(local.x, -halfSize.x, halfSize.x),
                    Mathf.Clamp(local.y, -halfSize.y, halfSize.y)
                );


            //Calculate distances from each edge
            float dx = Mathf.Abs(Mathf.Min(worldhalfSize.x - localNorm.x * ct.lossyScale.x, worldhalfSize.x + localNorm.x * ct.lossyScale.x));
            float dy = Mathf.Abs(Mathf.Min(worldhalfSize.y - localNorm.y * ct.lossyScale.y, worldhalfSize.y + localNorm.y * ct.lossyScale.y));



            // Select a face to project on
            if (dx < dy)
            {
                localNorm.x = Mathf.Sign(localNorm.x) * halfSize.x;
            }
            else
            {
                localNorm.y = Mathf.Sign(localNorm.y) * halfSize.y;
            }

            // Now we undo our transformations
            localNorm = localNorm + collider.offset;

            // Return resulting point
            return ct.TransformPoint(localNorm);
        }

        /// <summary>
        /// Find the closest point on the surface of a Polygon2D defined by the polygon collider 2D. Can be oriented.
        /// IS NOT OPTIMISED BY PARTITION
        /// </summary>
        /// <param name="collider"></param>
        /// <param name="toPosition"></param>
        /// <returns></returns>
        public static Vector2 ClosestPointOnSurface(PolygonCollider2D collider, Vector2 toPosition)
        {
            Vector2 positionReturned;
            float minDistance = Mathf.Infinity;

            // Cache the collider transform
            var ct = collider.transform;

            // Firstly, transform the point into the space of the collider
            Vector2 local = ct.InverseTransformPoint(toPosition);

            // Now, shift it to be in the center of the box
            local = local - collider.offset;

            int length = collider.points.Length;


            Vector2 firstPoint = collider.points[length - 1];
            Vector2 secondPoint = collider.points[0];

            Vector2 projectedPointOnSegment = ProjectPointOnLineSegment(firstPoint, secondPoint, local);
            minDistance = Vector2.SqrMagnitude(local - projectedPointOnSegment);
            positionReturned = projectedPointOnSegment;

            for (int i = 0; i < length-1; i++)
            {
                firstPoint = collider.points[i];
                secondPoint = collider.points[i+1];

                projectedPointOnSegment = ProjectPointOnLineSegment(firstPoint, secondPoint, local);

                float calc = Vector2.SqrMagnitude(local - projectedPointOnSegment);

                if (calc < minDistance)
                {
                    minDistance = calc;
                    positionReturned = projectedPointOnSegment;
                }
            }


            return (Vector2)ct.TransformPoint(positionReturned) + collider.offset;
        }


        public static Vector2 ProjectVectorOnLine(Vector2 lineDir, Vector2 vector)
        {
            return -(vector - (Vector2.Dot(vector, lineDir) * lineDir));
        }


        /// <summary>
        /// Return the projected point on a line. The projected point is the point on line that minimise the distance between him and the point parameter
        /// </summary>
        /// <param name="lineDir"></param>
        /// <param name="linePoint"></param>
        /// <param name="point"></param>
        /// <returns></returns>
        public static Vector2 ProjectPointOnLine(Vector2 lineDir, Vector2 linePoint, Vector2 point)
        {
            float calc = Vector2.Dot(point - linePoint, lineDir); ;

            return new Vector2(linePoint.x + calc * lineDir.x, linePoint.y + calc * lineDir.y);
        }


        //Get the shortest distance between a point and a line. The output is signed so it holds information
        public static float DistancePointToLine(Vector2 lineDir, Vector2 linePoint, Vector2 point)
        {
            Vector2 vector = linePoint - point;

            return Mathf.Abs(vector.x*lineDir.y - vector.y*lineDir.x);
        }


        /// <summary>
        /// create a vector of direction "vector" with length "size"
        /// </summary>
        /// <param name="vector"></param>
        /// <param name="size"></param>
        /// <returns></returns>
        public static Vector2 SetVectorLength(Vector2 vector, float size)
        {
            //normalize the vector
            return vector.normalized * size;
        }

        public static bool PointIsOnSegment(Vector2 firstPoint,Vector2 secondPoint, Vector2 pointToTest)
        {
            Vector2 dir1 = pointToTest - firstPoint;
            Vector2 dir2 = pointToTest - secondPoint;


            //Cross product tell us if all point are on the same line
            if( (dir1.x*dir2.y - dir1.y*dir2.x) < 0.0000001f)
            {
                //Dot product tell us if vector are or not oriented on the same way (with the sign)
                return Vector2.Dot(dir1, dir2) < 0;
            }

            return false;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="firstPoint"></param>
        /// <param name="secondPoint"></param>
        /// <param name="point"></param>
        /// <returns></returns>
        public static Vector2 ProjectPointOnLineSegment(Vector2 firstPoint, Vector2 secondPoint, Vector2 point)
        {
            Vector2 vector = secondPoint - firstPoint;

            Vector2 projectedPoint = ProjectPointOnLine(vector.normalized,firstPoint, point);

            Vector2 pointVec = point - firstPoint;

            float dot = Vector2.Dot(pointVec, vector);

            //point is on side of linePoint2, compared to linePoint1
            if (dot > 0)
            {
                //point is on the line segment
                if (pointVec.magnitude <= vector.magnitude)
                {
                    return projectedPoint;
                }
                //point is not on the line segment and it is on the side of linePoint2
                else
                {
                    return secondPoint;
                }
            }
            //Point is not on side of linePoint2, compared to linePoint1.
            //Point is not on the line segment and it is on the side of linePoint1.
            else
            {
                return firstPoint;
            }
        }



        /// <summary>
        /// This function returns a point which is a projection from a point to a plane.
        /// </summary>
        /// <param name="planeNormal"></param>
        /// <param name="planePoint"></param>
        /// <param name="point"></param>
        /// <returns></returns>
        public static Vector3 ProjectPointOnPlane(Vector3 planeNormal, Vector3 planePoint, Vector3 point)
        {

            float distance;
            Vector3 translationVector;

            //First calculate the distance from the point to the plane:
            distance = SignedDistancePlanePoint(planeNormal, planePoint, point);

            //Reverse the sign of the distance
            distance *= -1;

            //Get a translation vector
            translationVector = SetVectorLength(planeNormal, distance);

            //Translate the point to form a projection
            return point + translationVector;
        }



        /// <summary>
        ///Get the shortest distance between a point and a plane. The output is signed so it holds information
        ///as to which side of the plane normal the point is.
        /// </summary>
        /// <param name="planeNormal"></param>
        /// <param name="planePoint"></param>
        /// <param name="point"></param>
        /// <returns></returns>
        public static float SignedDistancePlanePoint(Vector3 planeNormal, Vector3 planePoint, Vector3 point)
        {
            return Vector3.Dot(planeNormal, (point - planePoint));
        }

        /// <summary>
        /// Calcul a CatmulRom spline interpolation with 4 given points.
        /// </summary>
        /// <param name="point0"></param>
        /// <param name="point1"></param>
        /// <param name="point2"></param>
        /// <param name="point3"></param>
        /// <param name="t"> interpolation parameter between point1 and point2</param>
        /// <returns>An interpolation point between point 2 and 3 </returns>
        public static Vector2 CatmullRomInterpolation(Vector2 point0, Vector2 point1, Vector2 point2, Vector2 point3,float t0, float t1, float t2, float t3, float t)
        {
            Vector2 A1 = ((t1 - t)  * point0 + (t - t0) * point1) / (t1 - t0);
            Vector2 A2 = ((t2 - t) * point1 + (t - t1) * point2) / (t2 - t1);
            Vector2 A3 = ((t3 - t) * point2 + (t - t2) * point3) / (t3 - t2);

            Vector2 B1 = ((t2 - t) * A1 + (t - t0) * A2) / (t2 - t0);
            Vector2 B2 = ((t3 - t) * A2 + (t - t1) * A3)/ (t3 - t1);

            return ((t2 - t) * B1 + (t - t1) * B2) / (t2 - t1);
        }


        public static void DrawCatmullRomInterpolation(Vector2 point0, Vector2 point1, Vector2 point2, Vector2 point3, float t0, float t1, float t2, float t3)
        {
            if (t2 != t1)
            {
                for (float t = t1; t <= t2; t += ((t2 - t1) / 50f))
                {
                    Vector2 A1 = ((t1 - t) * point0 + (t - t0) * point1) / (t1 - t0);
                    Vector2 A2 = ((t2 - t) * point1 + (t - t1) * point2) / (t2 - t1);
                    Vector2 A3 = ((t3 - t) * point2 + (t - t2) * point3) / (t3 - t2);

                    Vector2 B1 = ((t2 - t) * A1 + (t - t0) * A2) / (t2 - t0);
                    Vector2 B2 = ((t3 - t) * A2 + (t - t1) * A3) / (t3 - t1);

                    DebugDraw.DrawMarker(((t2 - t) * B1 + (t - t1) * B2) / (t2 - t1), 0.05f, Color.red, 2f);
                }
            }
        }

        public static void DrawLinearInterpolation(Vector3 point1, Vector2 point2)
        {
            for (float t = 0; t <= 1; t += 1f / 50f)
            {             
                DebugDraw.DrawMarker(Vector3.Lerp(point1,point2,t), 0.05f, Color.red, 2f);
            }
        }
    }


}
