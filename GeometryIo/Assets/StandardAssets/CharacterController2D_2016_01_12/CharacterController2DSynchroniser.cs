﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using System;

namespace EquilibreGames
{
public class CharacterController2DSynchroniser : MovementSynchronizer {

        class CharacterController2DState : State
        {
            public Quaternion rotation;
            public Vector3 velocity;

            public CharacterController2DState(CharacterController2D c, int timestamp)
            {
                position = c.CharacterTransform.position;
                rotation = c.CharacterTransform.rotation;
                velocity = c.velocity;
                this.timestamp = timestamp;
            }
        }


        [SerializeField]
        CharacterController2D characterController2D;

        [Space(10)]
        [SerializeField]
        SYNCHRONISATION_MODE positionSynchronizationMode;

        public INTERPOLATION_MODE positionInterpolationMode;

        [Space(20)]
        public float positionThreshold = 0.01f;
        public float snapThreshold = 10f;

        [Space(10)]
        [SerializeField]
        SYNCHRONISATION_MODE rotationSynchronizationMode;

        [SerializeField]
        [Tooltip("Angle in degree for minimum update")]
        float rotationThreshold = 0.1f;

        [Space(10)]
        [SerializeField]
        SYNCHRONISATION_MODE velocitySynchronizationMode;

        [SerializeField]
        [Tooltip("Don't do that if you always synchronise velocity")]
        bool calculVelocity = false;

        public SNAP_MODE velocitySnapMode = SNAP_MODE.CALCUL;

        [Space(20)]
        public bool useExtrapolation = false;
        public float extrapolationTime = 0.5f;

        private Vector3 lastPosition = Vector3.zero;
        private Vector3 lastVelocity = Vector3.zero;
        private Quaternion lastRotation;

       
        void Update()
        {
            this.IsActive = this.characterController2D.enabled;

            if (currentStatesIndex < 0 || networkIdentity.hasAuthority)
                return;

            State lhs;
            int lhsIndex;
            State rhs;
            float t;
            float firstStateDelay;
            Vector3 val = Vector3.zero;

            GetBestPlayBackState(out firstStateDelay,out lhsIndex, out lhs, out rhs, out t);

            //Extrapolation
            if (useExtrapolation && (lhs == null || rhs == null))
            {
                t = Mathf.Clamp(firstStateDelay - NetworkingSystem.Instance.synchronisationBackTime, 0,extrapolationTime);

                Vector3 acceleration = Vector3.zero;

                if (currentStatesIndex > 1)
                    acceleration = ((CharacterController2DState)statesBuffer[0]).velocity - ((CharacterController2DState)statesBuffer[1]).velocity;

                //VELOCITY
                val = characterController2D.velocity;

                if (!calculVelocity)
                    GetVector3(velocitySynchronizationMode, ref val, ((CharacterController2DState)statesBuffer[0]).velocity + acceleration * t);
                else if (currentStatesIndex > 1)
                    val =(statesBuffer[0].position - statesBuffer[1].position)/((statesBuffer[0].timestamp - statesBuffer[1].timestamp) /1000f);


                characterController2D.velocity = val;


                //POSITION
                val = characterController2D.CharacterTransform.position;

                GetVector3(positionSynchronizationMode, ref val, ((CharacterController2DState)statesBuffer[0]).position
                                                                    + ((CharacterController2DState)statesBuffer[0]).velocity * t
                                                                    + 0.5f * acceleration * t * t);
                characterController2D.CharacterTransform.position = val;


                //ROTATION
                val = characterController2D.CharacterTransform.rotation.eulerAngles;
               // GetVector3(rotationSynchronizationMode, ref val, ((CharacterController2DState)statesBuffer[0]).rotation);
                characterController2D.CharacterTransform.rotation = ((CharacterController2DState)statesBuffer[0]).rotation;
            }
            else if (lhs != null && rhs != null)
            {
                //POSITION
                bool snapPosition = false;
                lastPosition = characterController2D.CharacterTransform.position;

                val = characterController2D.CharacterTransform.position;
                if (Vector3.SqrMagnitude(((CharacterController2DState)rhs).position - ((CharacterController2DState)lhs).position) > (snapThreshold* snapThreshold))
                {
                    GetVector3(positionSynchronizationMode, ref val, ((CharacterController2DState)rhs).position);
                    snapPosition = true;
                }
                else
                {
                    INTERPOLATION_MODE interpolationMode = GetCurrentInterpolationMode(positionInterpolationMode, lhsIndex);

                    switch (interpolationMode)
                    {
                        case INTERPOLATION_MODE.LINEAR:
                            GetVector3(positionSynchronizationMode, ref val, Vector3.Lerp(lhs.position, rhs.position, t));
#if EQUILIBREGAMES_DEBUG
                            ExtendedMath.DrawLinearInterpolation(lhs.position, rhs.position);
#endif
                            break;
                        case INTERPOLATION_MODE.CATMULL_ROM:
                            GetVector3(positionSynchronizationMode, ref val, ExtendedMath.CatmullRomInterpolation(statesBuffer[lhsIndex + 1].position, lhs.position, rhs.position, statesBuffer[lhsIndex - 2].position,
                                                                             statesBuffer[lhsIndex + 1].timestamp, lhs.timestamp, rhs.timestamp, statesBuffer[lhsIndex - 2].timestamp, (1f-t)*lhs.timestamp + t*rhs.timestamp ));
#if EQUILIBREGAMES_DEBUG
                            ExtendedMath.DrawCatmullRomInterpolation(statesBuffer[lhsIndex + 1].position, lhs.position, rhs.position, statesBuffer[lhsIndex - 2].position,
                                                                             statesBuffer[lhsIndex + 1].timestamp, lhs.timestamp, rhs.timestamp, statesBuffer[lhsIndex - 2].timestamp);
#endif
                            break;
                    }
                }
                characterController2D.CharacterTransform.position = val;

                //ROTATION
                val = characterController2D.CharacterTransform.rotation.eulerAngles;
               // GetVector3(rotationSynchronizationMode, ref val, Vector3.Lerp(((CharacterController2DState)lhs).rotation, ((CharacterController2DState)rhs).rotation, t));
                characterController2D.CharacterTransform.rotation = Quaternion.Slerp(((CharacterController2DState)lhs).rotation, ((CharacterController2DState)rhs).rotation, t);

                //VELOCITY
                val = characterController2D.velocity;

                if (!calculVelocity)
                    GetVector3(velocitySynchronizationMode, ref val, Vector3.Lerp(((CharacterController2DState)lhs).velocity, ((CharacterController2DState)rhs).velocity, t));
                else if (currentStatesIndex > 1)
                {
                    if (snapPosition || (lhs.infos & (1<<1)) != 0)
                    {
                        switch (velocitySnapMode)
                        {
                            case SNAP_MODE.CALCUL: val = (((CharacterController2DState)rhs).position - ((CharacterController2DState)lhs).position) / ((rhs.timestamp - lhs.timestamp) / 1000f); break;
                            case SNAP_MODE.RESET: val = Vector3.zero; break;
                            default: break;
                        }
                    }
                    else
                        val = (((CharacterController2DState)rhs).position - ((CharacterController2DState)lhs).position) / ((rhs.timestamp - lhs.timestamp) / 1000f);
                }
                characterController2D.velocity = val;
            }
            else if (IsActive)
            {
                this.characterController2D.CharacterTransform.position = lastPosition;

                if (calculVelocity)
                    this.characterController2D.velocity = Vector3.zero;
                else
                    this.characterController2D.velocity = lastVelocity;

                this.characterController2D.CharacterTransform.rotation = lastRotation;
            }

            lastPosition = this.characterController2D.CharacterTransform.position;
            lastRotation = this.characterController2D.CharacterTransform.rotation;
            lastVelocity = this.characterController2D.velocity;
        }


        public override void ResetStatesBuffer()
        {
            base.ResetStatesBuffer();

            lastPosition = this.characterController2D.CharacterTransform.position;
            lastRotation = this.characterController2D.CharacterTransform.rotation;
            lastVelocity = this.characterController2D.velocity;
        }


        public override bool NeedToUpdate()
        {
            if ( (!base.NeedToUpdate()) ||
                (Time.realtimeSinceStartup < lastUpdateTime) ||
                ((Vector3.SqrMagnitude(characterController2D.CharacterTransform.position - lastPosition) < positionThreshold * positionThreshold)  && Quaternion.Angle(this.transform.rotation, lastRotation) < rotationThreshold) ||
                (!characterController2D.enabled) ) 
            return false;

            return true;
        }

        public override void GetCurrentState(NetworkWriter networkWriter)
        {
            lastPosition = characterController2D.CharacterTransform.position;
            lastRotation = characterController2D.CharacterTransform.rotation;

           SerializeVector3(positionSynchronizationMode, characterController2D.CharacterTransform.position, networkWriter);
           SerializeQuatenion(characterController2D.CharacterTransform.rotation, networkWriter);
           SerializeVector3(velocitySynchronizationMode, characterController2D.velocity, networkWriter);

            if (updateRate != 0)
                lastUpdateTime = Time.realtimeSinceStartup + 1f / updateRate;
            else
                lastUpdateTime = Time.realtimeSinceStartup + Mathf.Infinity;
        }

        public override void ReceiveCurrentState(int timestamp, NetworkReader networkReader)
        {
            CharacterController2DState newState = new CharacterController2DState(this.characterController2D, timestamp);

            UnserializeVector3(positionSynchronizationMode, ref newState.position, networkReader);
            UnserializeQuaternion(ref newState.rotation, networkReader);
            UnserializeVector3(velocitySynchronizationMode, ref newState.velocity, networkReader);

            AddState(newState);
        }
            
    }
}

