﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EquilibreGames;
using UnityEngine.UI;
using NodeCanvas.StateMachines;

public class Menu : MonoBehaviour {

    [SerializeField]
    InputField playerNameInputField;

    [SerializeField]
    FSMOwner fsm;

    void Awake()
    {
        playerNameInputField.text = PersistentDataSystem.Instance.GetSavedData<PlayerSavedData>().playerName;
    }

    public void ValidName()
    {
        if(! string.IsNullOrEmpty(playerNameInputField.text))
            fsm.TriggerState("JoinOrCreateGame");
    }

    public void JoinMatch()
    {
        if(MatchmakingSystem.Instance.m_LANMatchsAvailables.Count > 0)
        {
            MatchmakingSystem.Instance.ConnectToLANMatch(MatchmakingSystem.Instance.m_LANMatchsAvailables[0].serverAdress, MatchmakingSystem.Instance.m_LANMatchsAvailables[0].serverPort);
        }
    }

    public void CreateMatch()
    {
        MatchmakingSystem.Instance.CreateLANMatch("LaMereDeJulien");
    }

}
