﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using EquilibreGames;

public class PlayerNameDisplayer : MonoBehaviour {

    [SerializeField]
    Text playerNameText;

    void OnEnable()
    {
        playerNameText.text = PersistentDataSystem.Instance.GetSavedData<PlayerSavedData>().playerName;
    }

}
