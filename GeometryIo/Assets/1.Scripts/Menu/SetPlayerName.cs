﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using EquilibreGames;

public class SetPlayerName : MonoBehaviour {


    public void RegisterPlayerName(string s)
    {
        PersistentDataSystem.Instance.GetSavedData<PlayerSavedData>().playerName = s;
    }
}
