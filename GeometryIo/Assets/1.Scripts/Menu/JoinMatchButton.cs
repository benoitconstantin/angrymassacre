﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using EquilibreGames;

public class JoinMatchButton : MonoBehaviour {

    [SerializeField]
    Text matchNameText;

    void Update()
    {
        if (MatchmakingSystem.Instance.m_LANMatchsAvailables.Count > 0)
        {
            matchNameText.text = MatchmakingSystem.Instance.m_LANMatchsAvailables[0].name;
        }
    }

}
