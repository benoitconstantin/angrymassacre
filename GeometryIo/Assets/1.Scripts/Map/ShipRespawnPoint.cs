﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class ShipRespawnPoint : MonoBehaviour {

    public Vector2[] spawnposition;

    void Awake()
    {
       Ship.onShipRespawn += Respawn;
    }

    void OnDestroy()
    {
        Ship.onShipRespawn -= Respawn;
    }

    public void Respawn(Ship ship)
    {
        if(ship.hasAuthority)
            ship.shipmove.charactercontroller2D.CharacterTransform.position = spawnposition[Random.Range(0,spawnposition.Length)];
    }

    void OnDrawGizmos()
    {
        for (int i = 0; i < spawnposition.Length; i++)
        {
            Gizmos.DrawSphere(spawnposition[i], 1);
        }
    }
}
