﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NodeCanvas.Framework;
using UnityEngine.SceneManagement;

public class LoadSceneAction : ActionTask {

    public string sceneToLoad;
    public bool waitComplete = false;
    public bool setActiveScene = true;
    public bool ASync = true;

    protected override void OnExecute()
    {
        base.OnExecute();

        if(waitComplete)
             SceneManager.sceneLoaded += EndActionOnSceneLoad;

        if(ASync)
             SceneManager.LoadSceneAsync(sceneToLoad,LoadSceneMode.Additive);
        else
             SceneManager.LoadScene(sceneToLoad, LoadSceneMode.Additive);

        if (!waitComplete)
            EndAction();
    }

    void EndActionOnSceneLoad(Scene s, LoadSceneMode mode)
    {
        StartCoroutine(WorkAround(s));
        SceneManager.sceneLoaded -= EndActionOnSceneLoad;
    }

    IEnumerator WorkAround(Scene scene)
    {
        yield return new WaitForEndOfFrame();

        if (setActiveScene)
            SceneManager.SetActiveScene(scene);

        this.EndAction();
    }
}
