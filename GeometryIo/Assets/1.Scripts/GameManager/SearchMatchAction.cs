﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NodeCanvas.Framework;
using EquilibreGames;

public class SearchMatchAction : ActionTask {

    protected override void OnExecute()
    {
        base.OnExecute();
        MatchmakingSystem.Instance.StartListLANMatch();
    }

    protected override void OnStop()
    {
        base.OnStop();
        MatchmakingSystem.Instance.StopListMatch();
    }
}
