﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NodeCanvas.Framework;
using NodeCanvas.StateMachines;

public class GameManager : Singleton<GameManager> {
    
	void Awake()
    {
        DontDestroyOnLoad(this.gameObject);
    }
}
