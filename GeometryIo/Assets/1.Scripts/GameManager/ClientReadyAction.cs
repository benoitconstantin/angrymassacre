﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NodeCanvas.Framework;
using UnityEngine.Networking;
using EquilibreGames;

public class ClientReadyAction : ActionTask {

    protected override void OnExecute()
    {
        ClientScene.Ready(NetworkingSystem.Instance.client.connection);
        EndAction();
    }
}
