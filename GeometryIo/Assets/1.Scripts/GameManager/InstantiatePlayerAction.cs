﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NodeCanvas.Framework;
using EquilibreGames;
using UnityEngine.Networking;
using UnityEngine.Networking.NetworkSystem;

public class InstantiatePlayerAction : ActionTask {

    protected override void OnUpdate()
    {
        if (ClientScene.ready)
        {
            ClientScene.AddPlayer(0);
            this.EndAction();
        }
    }
}
