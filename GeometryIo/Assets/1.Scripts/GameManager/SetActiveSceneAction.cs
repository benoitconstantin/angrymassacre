﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NodeCanvas.Framework;
using UnityEngine.SceneManagement;

public class SetActiveSceneAction : ActionTask {

    public string sceneName;

    protected override void OnExecute()
    {
        SceneManager.SetActiveScene(SceneManager.GetSceneByName(sceneName));
        EndAction();
    }
}
