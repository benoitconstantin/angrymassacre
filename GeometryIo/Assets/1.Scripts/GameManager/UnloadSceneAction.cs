﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NodeCanvas.Framework;
using UnityEngine.SceneManagement;

public class UnloadSceneAction : ActionTask
{
    public string unloadSceneName;
    public bool waitComplete = false;

    protected override void OnExecute()
    {
        if(waitComplete)
            SceneManager.sceneUnloaded += EndActionOnSceneUnLoad;

        SceneManager.UnloadSceneAsync(unloadSceneName);

        if(!waitComplete)
             EndAction();
    }


    void EndActionOnSceneUnLoad(Scene s)
    {
        this.EndAction();
        SceneManager.sceneUnloaded -= EndActionOnSceneUnLoad;
    }
}
