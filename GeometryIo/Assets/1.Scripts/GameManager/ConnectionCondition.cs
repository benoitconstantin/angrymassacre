﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NodeCanvas.Framework;
using EquilibreGames;

public class ConnectionCondition : ConditionTask {

    protected override bool OnCheck()
    {
        return MatchmakingSystem.IsOnLanMatch;
    }
}
