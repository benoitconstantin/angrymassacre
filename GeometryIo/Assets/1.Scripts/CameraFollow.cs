﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour {
    [SerializeField]
    Transform cameratransform;
	
	// Update is called once per frame
	void Update () {
		if (Ship.playableShip)
        {
            cameratransform.position = new Vector3 (Ship.playableShip.shipmove.charactercontroller2D.CharacterTransform.position.x, Ship.playableShip.shipmove.charactercontroller2D.CharacterTransform.position.y, cameratransform.position.z);
        }
	}
}
