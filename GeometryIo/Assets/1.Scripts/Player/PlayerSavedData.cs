﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EquilibreGames;

[System.Serializable]
public class PlayerSavedData : SavedData {

    public string playerName;
    
}
