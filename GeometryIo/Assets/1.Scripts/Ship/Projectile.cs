﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour {

    public Rigidbody2D rgdbody2D;
    public Transform tr;
    public byte index = 0;
    public Fire fire;

	void OnTriggerEnter2D(Collider2D c)
    {
        ShipReference shipRef = c.gameObject.GetComponent<ShipReference>();
        Projectile p = c.gameObject.GetComponent<Projectile>();
        IAReference iaRef = c.gameObject.GetComponent<IAReference>();

        if (shipRef)
        {
            if (shipRef.ship.fire != fire)
            {
                if (shipRef.ship.Hitten())
                {
                    fire.StackProjectile(this);
                }
            }
        }
        else if (p)
        {
            if (p.fire != fire)
            {
                fire.StackProjectile(p);
                p.fire.StackProjectile(p);
            }
        }
        else if (iaRef)
        {
            iaRef.ia.Hit();
            fire.StackProjectile(this);
        }
        else
            fire.LocalStackProjectile(this);
    }

}
