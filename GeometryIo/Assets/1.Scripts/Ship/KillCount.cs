﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using EquilibreGames;
using UnityEngine.Networking;
using UnityEngine.Networking.NetworkSystem;

public class KillCount : MonoBehaviour {

    int killcount = 0;

    [SerializeField]
    Text text;

	// Use this for initialization
	void Awake () {
        text.text = "Number of kills : " + killcount;
        Ship.onShipDead += CountIncrement;

        if (NetworkServer.active)
            NetworkingSystem.OnClientReadyOnServer += SendNumberOfKills;

        NetworkingSystem.Instance.client.RegisterHandler(NetworkMessages.NumberOfKillsSync, SyncNumberOfKills);
	}

    void OnDestroy()
    {
        Ship.onShipDead -= CountIncrement;
        NetworkingSystem.OnClientReadyOnServer -= SendNumberOfKills;
    }

    public void CountIncrement(Ship ship)
    {
        killcount++;
        text.text = "Number of kills : " + killcount;
    }

    void SyncNumberOfKills(NetworkMessage netMsg)
    {
        killcount = netMsg.ReadMessage<IntegerMessage>().value;
        text.text = "Number of kills : " + killcount;
    }

    void SendNumberOfKills(NetworkMessage netMsg)
    {
        NetworkServer.SendToAll(NetworkMessages.NumberOfKillsSync, new IntegerMessage(killcount));
    }
}
