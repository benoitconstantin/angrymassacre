﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using EquilibreGames;


public class ColorSynchronizer : NetworkBehaviour {

    [SerializeField]
    RandomSpriteColor[] randomSpriteColors;

    public override void OnStartAuthority()
    {
        base.OnStartAuthority();

        for (int i = 0; i < randomSpriteColors.Length; i++)
        {
            randomSpriteColors[i].SetColor();
        }

        SendShipColors(null);
        NetworkingSystem.OnClientReadyFromServer += SendShipColors;
    }

    void OnDestroy()
    {
        NetworkingSystem.OnClientReadyFromServer -= SendShipColors;
    }

    void SendShipColors(NetworkMessage netMSg)
    {
        Color[] colors = new Color[randomSpriteColors.Length];

        for(int i =0; i < randomSpriteColors.Length; i++)
        {
            colors[i] = (randomSpriteColors[i].color);
        }

        if (isServer)
            RpcSendShipColors(colors);
        else
            CmdSendShipColors(colors);
    }

    [ClientRpc]
    void RpcSendShipColors(Color[] colors)
    {
        for (int i = 0; i < randomSpriteColors.Length; i++)
        {
            randomSpriteColors[i].SetColor(colors[i]);
        }
    }

    [Command]
    void CmdSendShipColors(Color[] colors)
    {
        RpcSendShipColors(colors);
    }

}
