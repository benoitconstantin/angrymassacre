﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EquilibreGames;
using NodeCanvas.Framework;

public class StopWhenDeadAction : ActionTask {

    [BlackboardOnly]
    public BBParameter<CharacterController2D> Controller2D;

    protected override void OnExecute()
    {
        Controller2D.value.acceleration = Vector2.zero;
        Controller2D.value.velocity = Vector3.zero;
        EndAction();
    }
}
