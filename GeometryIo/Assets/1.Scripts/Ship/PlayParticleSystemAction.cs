﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NodeCanvas.Framework;


public class PlayParticleSystemAction : ActionTask {
    
    [BlackboardOnly]
    public BBParameter<ParticleSystem> particleSystem;

    protected override void OnExecute()
    {
        particleSystem.value.Play();
        EndAction();
    }
}
