﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NodeCanvas.Framework;
using UnityEngine.Networking;

public class FireAction : ActionTask {

    Fire shipfire;
    NetworkIdentity netid;
    protected override string OnInit()
    {
        shipfire = blackboard.GetValue<Fire>("Fire");
        netid = blackboard.GetValue<NetworkIdentity>("NetworkIdentity");

        return base.OnInit();
    }

    protected override void OnExecute()
    {
        base.OnExecute();
        if(netid.hasAuthority)
        {
            shipfire.Shoot();
        }

        EndAction();
    }
}
