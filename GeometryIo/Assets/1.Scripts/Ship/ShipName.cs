﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using EquilibreGames;


public class ShipName : NetworkBehaviour {

    [SerializeField]
    Text nameText;

    public override void OnStartAuthority()
    {
        base.OnStartAuthority();

        nameText.text = PersistentDataSystem.Instance.GetSavedData<PlayerSavedData>().playerName;
        SendName(null);
        NetworkingSystem.OnClientReadyFromServer += SendName;
    }

    void OnDestroy()
    {
        NetworkingSystem.OnClientReadyFromServer -= SendName;
    }

    void SendName(NetworkMessage netMsg)
    {
        if (isServer)
            RpcSendName(nameText.text);
        else
            CmdSendName(nameText.text);
    }

    [ClientRpc]
    void RpcSendName(string name)
    {
        nameText.text = name;
    }

    [Command]
    void CmdSendName(string name)
    {
        RpcSendName(name);
    }

}
