﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EquilibreGames;

public class ShipMove : MonoBehaviour {

    public CharacterController2D charactercontroller2D;
    public float speedvalue = 15.0f;
    public float maxspeed = 90.0f;
    public float decelerationvalue = 0.5f;
 


	public void BasicMove (float inputX , float inputY) {

        charactercontroller2D.acceleration.x = inputX * speedvalue;
        charactercontroller2D.acceleration.y = inputY * speedvalue;
        if(charactercontroller2D.GetNextVelocityValue(Time.fixedDeltaTime).magnitude>maxspeed)
        {
            charactercontroller2D.acceleration.x = 0.0f;
            charactercontroller2D.acceleration.y = 0.0f;
            charactercontroller2D.velocity = charactercontroller2D.velocity.normalized * maxspeed;
        }
    }

    public void Deceleration()
    {
        charactercontroller2D.acceleration = -charactercontroller2D.velocity * decelerationvalue;
    }
}