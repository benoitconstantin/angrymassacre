﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NodeCanvas.Framework;
using UnityEngine.Networking;

public class DeadAction : ActionTask {

    Ship deadship;
    Transform shiptransform;

    protected override string OnInit()
    {
        shiptransform = blackboard.GetValue<Transform>("Appearence");
        deadship = blackboard.GetValue<Ship>("Ship");

        return base.OnInit();
    }

    protected override void OnExecute()
    {
        shiptransform.gameObject.SetActive(false);

        if (Ship.onShipDead != null)
        {
            Ship.onShipDead(deadship);
        }
    }

    protected override void OnStop()
    {
        base.OnStop();
        shiptransform.gameObject.SetActive(true);
    }
}
