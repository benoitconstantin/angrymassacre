﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using NodeCanvas.StateMachines;
using System;

public class Ship : NetworkBehaviour {

    public static List<Ship> ships = new List<Ship>();
    public static Ship playableShip;
    public static Action<Ship> onShipRespawn;
    public static Action<Ship> onShipDead;

    public ShipMove shipmove;
    public FSMOwner statemachine;
    public Fire fire;
    public ShipAppearence shipappearence;

    void Awake()
    {
        ships.Add(this);
    }

    void OnDestroy()
    {
        ships.Remove(this);

        if (hasAuthority)
            playableShip = null;
    }

    public override void OnStartAuthority()
    {
        base.OnStartAuthority();

        playableShip = this;
    }

    public void LocalHitten()
    {
        if (!statemachine.currentStateName.Equals("Dead"))
        {
            statemachine.TriggerState("Dead");
        }
    }

    public bool Hitten()
    {
        if(!isServer)
        {
            return false;
        }

        LocalHitten();
        RpcHitten();

        return true;
    }

    [ClientRpc]
    void RpcHitten()
    {
        if (!isServer)
            LocalHitten();
    }

}
