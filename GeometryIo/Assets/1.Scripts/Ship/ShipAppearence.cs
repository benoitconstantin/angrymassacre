﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class ShipAppearence : NetworkBehaviour {
    [SerializeField]
    Ship ship;

    [SerializeField]
    float rotationSpeed;

    void FixedUpdate()
    {
        if (hasAuthority && Camera.main)
        {
            Vector3 lookAt = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, -Camera.main.transform.position.z)) - ship.shipmove.charactercontroller2D.CharacterTransform.position;
            lookAt.z = 0;


            ship.shipmove.charactercontroller2D.CharacterTransform.rotation = Quaternion.Slerp(ship.shipmove.charactercontroller2D.CharacterTransform.rotation,
                Quaternion.Euler(0, 0, lookAt.y > 0 ? Vector2.Angle(Vector2.right, lookAt) : -Vector2.Angle(Vector2.right, lookAt)),
                rotationSpeed * Time.fixedDeltaTime);
        }
    }
	
}
