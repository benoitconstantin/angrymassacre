﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NodeCanvas.Framework;
using UnityEngine.Networking;

public class RespawnAction : ActionTask
{
    Ship shiprespawn;

    protected override string OnInit()
    {
        shiprespawn = blackboard.GetValue<Ship>("Ship");

        return base.OnInit();
    }

    protected override void OnExecute()
    {
        base.OnExecute();

        if (Ship.onShipRespawn != null)
        {
            Ship.onShipRespawn(shiprespawn);
        }

        EndAction();
    }
}  