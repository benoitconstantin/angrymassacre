﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NodeCanvas.Framework;
using UnityEngine.Networking;

public class MoveAction : ActionTask {
    ShipMove shipmove;
    NetworkIdentity netid;
    protected override string OnInit()
    {
        shipmove = blackboard.GetValue<ShipMove>("ShipMove");
        netid = blackboard.GetValue<NetworkIdentity>("NetworkIdentity");

        return base.OnInit();
    }
    protected override void OnUpdate()
    {
        if (netid.hasAuthority)
        {
            if (Input.GetMouseButton(0))
            {
                Vector3 dirVector = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, -Camera.main.transform.position.z)) - shipmove.charactercontroller2D.CharacterTransform.position;
                dirVector.Normalize();
                shipmove.BasicMove(dirVector.x, dirVector.y);
            }
            else if (Input.GetAxis("MoveX") != 0 || Input.GetAxis("MoveY") != 0)
            {
                shipmove.BasicMove(Input.GetAxis("MoveX"), Input.GetAxis("MoveY"));
            }
            else
            {
                shipmove.Deceleration();
            }
        }
    }
}
