﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class Fire : NetworkBehaviour
{
    [SerializeField]
    Projectile[] ammo;

    LinkedList<Projectile> availableprojectile = new LinkedList<Projectile>();
    LinkedList<Projectile> unavailableprojectile = new LinkedList<Projectile>();

    [SerializeField]
    Transform visee;

    public float firespeed = 10.0f;

    void Awake()
    {
        for (byte i = 0; i < ammo.Length; i++)
        {
            availableprojectile.AddLast(ammo[i]);
            ammo[i].gameObject.SetActive(false);
            ammo[i].fire = this;
            ammo[i].index = i;
        }
    }

    public void LocalShoot(byte index, Vector3 position, Quaternion rotation)
    {
        Projectile p = ammo[index];

        p.tr.position = position;
        p.tr.rotation = rotation;
        p.gameObject.SetActive(true);
        p.rgdbody2D.velocity = visee.up * firespeed;
        unavailableprojectile.AddLast(availableprojectile.First.Value);
        availableprojectile.Remove(p);
    }

    public void Shoot()
    {
        if (!hasAuthority)
            return;

        if (isServer)
            RpcShoot(availableprojectile.First.Value.index, visee.position, visee.rotation);
        else
            CmdShoot(availableprojectile.First.Value.index, visee.position, visee.rotation);

        LocalShoot(availableprojectile.First.Value.index, visee.position, visee.rotation);
    }

    [ClientRpc]
    void RpcShoot(byte index, Vector3 position, Quaternion rotation)
    {
        if (hasAuthority)
            return;

        LocalShoot(index, position, rotation);
    }

    [Command]
    void CmdShoot(byte index, Vector3 position, Quaternion rotation)
    {
        RpcShoot(index, position, rotation);
    }


    public void StackProjectile(Projectile p)
    {
        if (!isServer)
            return;

        LocalStackProjectile(p);

        for (int i = 0; i < ammo.Length; i++)
        {
            if (ammo[i] == p)
                RpcStackProjectile(i);
        }
    }

    public void LocalStackProjectile(Projectile p)
    {
        p.gameObject.SetActive(false);
        unavailableprojectile.Remove(p);
        availableprojectile.AddLast(p);
    }

    [ClientRpc]
    public void RpcStackProjectile(int index)
    {
        LocalStackProjectile(ammo[index]);
    }

}

