﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NodeCanvas.Framework;
using EquilibreGames;

public class MoveIA : ActionTask {

    public float speed = 1;

    CharacterController2D characterController2D;

    protected override string OnInit()
    {
        characterController2D = blackboard.GetValue<CharacterController2D>("characterController2D");
        return base.OnInit();
    }

    protected override void OnExecute()
    {
        characterController2D.velocity = blackboard.GetValue<Vector2>("direction").normalized * speed;
        EndAction();
    }
}
