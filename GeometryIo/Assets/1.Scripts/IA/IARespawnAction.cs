﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NodeCanvas.Framework;

public class IARespawnAction : ActionTask {

    [BlackboardOnly]
    public BBParameter<IA> ia;

    protected override void OnExecute()
    {
        if(IA.OnIARespawn != null)
            IA.OnIARespawn(ia.value);

        EndAction();
    }


}
