﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NodeCanvas.Framework;
using EquilibreGames;


public class WallDetection : ConditionTask {

    public float distance;
    public LayerMask layerMask;

    CharacterController2D characterController2D;
    Transform origin;

    protected override string OnInit()
    {
        characterController2D = blackboard.GetValue<CharacterController2D>("characterController2D");
        origin = blackboard.GetValue<Transform>("origin");

        return base.OnInit();
    }

    protected override bool OnCheck()
    {
        Vector2 direction = blackboard.GetValue<Vector2>("direction");

        if (direction == Vector2.zero)
            return true;

        if( Physics2D.Raycast(origin.position, direction, distance, layerMask))
        {
            blackboard.SetValue("direction", -direction);
            return true;
        }

        for(int i = 0; i < characterController2D.layersHitten.Count; i++)
        {
            if( ((1<< characterController2D.layersHitten[i]) & layerMask) != 0)
            {
                blackboard.SetValue("direction", -direction);
                return true;
            }
        }

        return false;

    }

}
