﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NodeCanvas.Framework;

public class GameObjectActiveAction : ActionTask {

    [BlackboardOnly]
    public BBParameter<GameObject> gameObject;
    public bool active = false;

    protected override void OnExecute()
    {
        gameObject.value.SetActive(active);
        EndAction();
    }

}
