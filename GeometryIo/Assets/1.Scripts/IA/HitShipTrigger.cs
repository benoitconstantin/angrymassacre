﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitShipTrigger : MonoBehaviour {


    void OnTriggerEnter2D(Collider2D c)
    {
        ShipReference shipRef = c.gameObject.GetComponent<ShipReference>();

        if(shipRef)
        {
            shipRef.ship.Hitten();
        }
    }

}
