﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateTransform : MonoBehaviour {

    [SerializeField]
    Transform tr;

    public float rotateSpeed;

    void FixedUpdate()
    {
        tr.rotation = Quaternion.Euler(0,0 , tr.rotation.eulerAngles.z + rotateSpeed * Time.fixedDeltaTime);
    }
}
