﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NodeCanvas.Framework;


public class ChoseDirection : ActionTask {

    public bool randomDirection = true;


    protected override void OnExecute()
    {
        if (randomDirection)
            blackboard.SetValue("direction", new Vector2(Random.Range(-1f, 1f), Random.Range(-1f, 1f)));
        else
        {
            Vector2 direction = blackboard.GetValue<Vector2>("direction");
            float magnitude = direction.magnitude;
            float memory = direction.x;

            if (Random.Range(0f, 1f) > 0.5f)
            {
                direction.x = -direction.y / magnitude;
                direction.y = memory / magnitude;
            }
            else
            {
                direction.x = direction.y / magnitude;
                direction.y = -memory / magnitude;
            }

            blackboard.SetValue("direction", direction);
        }

        this.EndAction();
    }


}
