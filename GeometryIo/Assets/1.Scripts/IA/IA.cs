﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using NodeCanvas.Framework;
using NodeCanvas.BehaviourTrees;
using System;
using EquilibreGames;



public class IA : NetworkBehaviour {

    public static Action<IA> OnIARespawn;

    [SerializeField]
    BehaviourTreeOwner bht;

    public CharacterController2D characterController2D;

    public void LocalHit()
    {
        if (!bht.blackboard.GetValue<bool>("dead"))
        {
            bht.blackboard.SetValue("dead", true);
        }
    }

    public bool Hit()
    {
        if (!isServer)
        {
            return false;
        }

        LocalHit();
        RpcHit();

        return true;
    }

    [ClientRpc]
    void RpcHit()
    {
        if (!isServer)
            LocalHit();
    }


}
