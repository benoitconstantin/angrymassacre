﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using EquilibreGames;

public class IASpawner : MonoBehaviour {

    [SerializeField]
    GameObject prefab;

    [SerializeField]
    int numberOfIA;

    [SerializeField]
    Vector2[] spawnPositions;

    void Start()
    {
        if (NetworkServer.active)
        {
            SpawnIA(null);

            IA.OnIARespawn += Replace;
        }

        NetworkingSystem.OnStartServer += SpawnIA;
    }

    void OnDestroy()
    {
        NetworkingSystem.OnStartServer -= SpawnIA;
        IA.OnIARespawn -= Replace;
    }


    void Replace(IA ia)
    {
        ia.characterController2D.CharacterTransform.position = spawnPositions[Random.Range(0, spawnPositions.Length)];
    }


    void SpawnIA(NetworkMessage netMsg)
    {
        for (int i = 0; i < numberOfIA; i++)
        {
            GameObject obj = Instantiate(prefab);
            obj.GetComponent<CharacterController2D>().CharacterTransform.position = spawnPositions[Random.Range(0, spawnPositions.Length)];
            NetworkServer.Spawn(obj);
        }
    }

    void OnDrawGizmos()
    {
        Gizmos.color = Color.red;

        for (int i = 0; i < spawnPositions.Length; i++)
        {
            Gizmos.DrawSphere(spawnPositions[i], 1);
        }
    }

}
